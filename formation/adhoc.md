# Ansible Ad HOC

## Pattern
```bash
ansible [pattern] -m [module] -a "[module options]"
```

## Module Ping
```bash
ansible hostname -m ping
```

## Module Command - get uptime
```bash
ansible groupname -i inventory -m command -a uptime 
ansible hostname -a uptime
ansible hostname -m shell -a uptime 
```

## Module Command - freemem
```bash
ansible groupname -a "free -m" -i inventory
```

## Module Yum
```bash
ansible webserver -m ansible.builtin.yum -a "name=nginx state=present"
``` 

## Module Apt
```bash
ansible webserver -m ansible.builtin.apt -a "name=nginx state=present"
```

## Generic OS package manager
```bash
ansible webserver -m ansible.builtin.package -a "name=nginx state=present"
```
[Ansible generic os package manager parameters](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html#parameters)


## Module Cron
```bash
#  Run the job every 15 minutes
$ ansible multi -s -m cron -a "name='daily-cron-all-servers' minute=*/15 
job='/path/to/minute-script.sh'"

# Run the job every four hours
$ ansible multi -s -m cron -a "name='daily-cron-all-servers' hour=4 
job='/path/to/hour-script.sh'"

# Enabling a Job to run at system reboot
$ ansible multi -s -m cron -a "name='daily-cron-all-servers' special_time=reboot 
job='/path/to/startup-script.sh'"

# Scheduling a Daily job
$ ansible multi -s -m cron -a "name='daily-cron-all-servers' special_time=daily 
job='/path/to/daily-script.sh'"

# Scheduling a Weekly job
$ ansible multi -s -m cron -a "name='daily-cron-all-servers' special_time=weekly 
job='/path/to/daily-script.sh'"
```

## Sources
* [Ansible adhoc Intro](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)
* [Ansible patternes Intro](https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html#intro-patterns)
* [Ansible managing package](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#managing-packages)