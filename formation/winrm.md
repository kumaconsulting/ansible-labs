# Ansible with Windows Hosts

Installation de WinRM afin que Ansible puisse communiquer avec le Windows **Managed Host**

## Installer WinRM
La Team Ansible nous a facilité le travail en préparant un [script PowerShell](https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1)

Lancez la commande suivante en tant qu'Administrateur dans un terminal PowerShell.
```powershell
iex(iwr https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1).Content
```
![winrm install](/images/AnsibleWinRM.webp)

Vérifiez que le process est en écoute sur le port 5986
```powershell
netstat -anp|findstr 5986
```

## Sur le Control Node
Sur la machine qui lance les playbook, il faut installer pywinrm

```bash
pip3 install pywinrm
```

Testons la bonne communication entre les deux machines
```bash
nc -w 3 -v <remote windows server ip/hostname> 5986
```

## Exemple de déclaration dans un inventory
```toml
[win]
192.168.12.122

[win:vars]
ansible_connection=winrm 
ansible_user=administrator 
ansible_password=r$eBQNgc5U&A2at8kDwpWo.KzLT5NvHd 
ansible_winrm_server_cert_validation=ignore
```

    :warning: il est préférable de déclarer un compte de service dédié à Ansible.