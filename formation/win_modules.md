# Ansible pour Windows

## Liste des modules


## Win_copy
Permet de copier des fichiers vers une machine Windows.

https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_copy_module.html

## Win_file
Permet de supprimer/créer des fichiers/dossier sur une machine Windows.

https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_file_module.html

