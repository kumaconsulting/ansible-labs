# Installer Vagrant sur Windows10

Installation de vagrant + virtualbox sur un Windows10

Pour les étapes ci-dessous, veuillez ouvrir une fenêtre PowerShell en tant qu'Administrateur.

## VirtualBox

Désactivation de Hyper-V:
```powershell
Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All

```

Installation de chocolatey:
```powershell
Set-ExecutionPolicy Bypass -Force;

iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

```powershell
choco install -y virtualbox
```

## Install Vagrant
```powershell
choco install -y vagrant
```

Redémarrez votre machine Windows.

Ré-ouvrez un PowerShell en tant qu'Administrateur, puis tapez les commandes suivantes:
```powershell
vagrant plugin install vagrant-uplift
vagrant plugin install vagrant-reload
```

## Provisionning Ubuntu 20.04 LTS

```powershell
vagrant init bento/ubuntu-20.04
vagrant up
```