# VAULT EXAMPLES


## Contenu fichier secrets/lab.vault

```yaml
monvar: "titi"
vault_ssh_pubkey_jcu: |
  ssh-rsa AAAAB3Nz[..]8wv5jIAj5zX

vault_id_rsa_piv: |
  -----BEGIN RSA PRIVATE KEY-----
  ergergerg
  ergergerg
  [...]
  -----END RSA PRIVATE KEY-----
```

## Chiffrage du fichier
```bash
ansible-vault encrypt secrets/lab.yml
```

## Déchiffrage du fichier
```bash
ansible-vault decrypt secrets/lab.yml
```

## Appel dans un Playbook
````yaml
---
- hosts: all
  become: true
  vars_files:
    - "secrets/{{ env }}.vault"

  tasks:
    - name: Debug tasks
      debug:
        msg: "Contenu de mon var: {{ monvar }}"

````

Commande:
```bash
ansible-playbook -i inventory.ini playbook.yml -e "env=lab"
```
# Ansible Sources
* [vault](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_vault.html)