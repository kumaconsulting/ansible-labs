# ansible-labs

Formation ANSIBLE

## Environnement de formation - Préparation

### Installation paquet curl

```bash
sudo apt update
sudo apt install -y curl
```

### Installation paquets python3
```bash
sudo apt install -y python3 python3-pip
```

### Installation paquet git
```bash
sudo apt install -y git
```

### Télécharger le repo GIT
```bash
git clone https://gitlab.com/kumaconsulting/ansible-labs.git
```

### Installation Ansible
```bash
cd ansible-labs/
./install_ansible.sh
```

## Navigation
* [Accueil - Ansible Labs](/)
* [Les travaux pratiques (TP)](/TP/README.md)


## Sources documentaires
* [Ansible documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-debian)
* [Windows WSL2](https://docs.microsoft.com/fr-fr/windows/wsl/install)
* [Origins of Ansible](https://www.ansible.com/blog/2013/12/08/the-origins-of-ansible)

## Autheur
©2022 Kuma-Consulting - Jérémie CUADRADO (redbeard28)

## Licence
GPLv3

Toutes les marques citées appartiennent à leurs propriétaires; marques de tiers, noms de produits, noms commerciaux, dénominations sociales et noms de société cités sont des marques commerciales de leurs propriétaires respectifs ou des marques déposées d'autres sociétés et ne sont utilisés que pour des fins d'illustration et au bénéfice de leur propriétaire, sans que cela n'implique d'aucune manière une violation du droit d'auteur. 

