#!/bin/bash
# kuma-consulting

echo "Installing pre-requisits"
sudo apt update
sudo apt upgrade -y
sudo apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common

echo "Add PPA Ansible source list"
sudo echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" > /etc/apt/sources.list.d/ansible.list
echo "Add APT Key from ubuntu.com"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
echo "Update packages list"
sudo apt update
echo "Install Ansible from PPA repo"
sudo apt install ansible
#
#
echo "Install some other packages for th labs"
sudo apt install -y ncat
