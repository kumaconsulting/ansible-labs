# Ansible Labs - Liste des TP

Vous trouverez ci-dessous la liste de TP disponibles.

## Ansible ad hoc
* [TP-1: Ansible ad hoc](/TP/TP-1.md)
## Playbook
* [TP-2: Premier playbook](/TP/TP-2.md)
## Installation de packaged
* [TP-3: Installation nginx](/TP/TP-3.md)
##
* [TP-4:](/TP/TP-4.md)


## Navigation
* [Accueil - Ansible Labs](/)