# TP-3 - Installer le package nginx

## Inventory
Creez le fichier inventory.ini
Convenez de prendre un des deux instances ;-)

```ini
[all]
kumalabs_instance01 ansible_host=212.47.233.59
kumalabs_instance02 ansible_host=163.172.131.241

[managed-nodes]
kumalabs_instance01
kumalabs_instance02

[managed-nodes:vars]
ansible_user=root

```

'*' Remplacer par les valeurs de l'environnement (voir le formateur)


## Créez un fichier playbook.yml
```yaml
- hosts: all
  vars:
    myuser: "espace1" # ou espace2

  tasks:
  - name: Add my pubkeys
    authorized_key:
      user: "{{ myuser }}"
      state: present
      key: '{{ item }}'
    with_file:
      - "{{ path_ssh_pub }}"
    tags: keys
```

Lancez le playbook:
```bash
ansible-playbook -i inventory.ini playbook.yml -k
```

## Créez un fichier playbook.yml
```yaml
- hosts: all
  become: true
  vars:
    myuser: "espace1" # ou espace2

  tasks:
  - name: Add my pubkeys
    authorized_key:
      user: "{{ myuser }}"
      state: present
      key: '{{ item }}'
    with_file:
      - "{{ path_ssh_pub }}"
    tags: keys

  - name: Install package nginx
    package:
      name: nginx
      state: present

```

Lancez le playbook:
```bash
ansible-playbook -i inventory.ini playbook.yml
```

## Source documentaire
* [Ansible.builtin.package](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html)


## Navigation
* [Accueil - Ansible Labs](/README.md)
