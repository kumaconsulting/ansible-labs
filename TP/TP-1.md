# TP-1 - Ansible Ad hoc

## Pré-requis
* [Ansible doit être installé](../README.md)

## Vérifiez qu'Ansible est bien installé
Tapez la commande suivante:
```bash
ansible -version
```

Réponse attendu:
```bash
ansible 2.10.5
  config file = None
  configured module search path = ['/home/jeremie/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/local/lib/python3.7/dist-packages/ansible
  executable location = /usr/local/bin/ansible
  python version = 3.7.3 (default, Jan 22 2021, 20:04:44) [GCC 8.3.0]
```

## Lancez votre première commande Ad Hoc
Tapez ceci:
```bash
ansible localhost -m ping
```

Réponse attendu:
```bash
[WARNING]: No inventory was parsed, only implicit localhost is available
localhost | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

## Mémoire RAM disponible
Interrogez le/les managed node(s) pour connaître la mémoire vive disponible.

## Espace disque
Interrogez le/les managed node(s) pour connaître l'espace disque disponible.

## Installez un package
Veuillez installer le package **jq** sur le/les managed node(s).

Résultat:
```bash
webserver | CHANGED => {
    "cache_update_time": 1646956802,
    "cache_updated": false,
    "changed": true,
    "stderr": "",
    "stderr_lines": [],
    [...]
}
```
## Sources Documentaires
* [Ansible Ad Hoc](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)

## Navigation
* [Accueil - Ansible Labs](/README.md)