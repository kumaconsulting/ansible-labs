# TP-2 - Premier Playbook

## Pré-requis
* [Ansible doit être installé](../README.md)

## Générez vos clefs SSH
```bash
ssh-keygen -t rsa -b 4096
```

:warning: - Ne mettez pas de mot de passe pour la clef id_rsa

## Creez les répertoires suivants
* myplaybook
  * group_vars

```bash
mkdir -p myplaybook/group_vars
```

## Group_vars

copiez/collez ceci dans **myplaybook/group_vars/all.yml**
```yaml
---
ansible_managed: "Ansible managed config"
path_ssh_pub: "~/.ssh/id_rsa.pub"
```

## Fichier ansible.cfg
Creez un fichier **myplaybook/ansible.cfg**
```ini
[defaults]
interpreter_python = /usr/bin/python3
roles_path = roles:external_roles
#vault_password_file = .vault_password
host_key_checking = False
#ask_vault_pass =True
log_path = ~/tmp/ansible.log
local_tmp = ~/tmp/.ansible/tmp
retry_files_enabled = True
retry_files_save_path = ~/tmp/.ansible/ansible-retry
force_color = 1
forks = 60
callback_whitelist = profile_tasks
timeout = 60
[ssh_connection]
ansible_connection=ssh
ssh_args = -o ControlMaster=auto -o ControlPersist=30m
control_path = ~/.ssh/ansible-%%r@%%h:%%p
scp_if_ssh = True


```

## Créez un fichier playbook.yml
```yaml
- hosts: all
  vars:
    myuser: "espace1" # ou espace2

  tasks:
  - name: Get Hostname
    shell: |
      hostname
    register: myvar

  - debug:
      msg: "This is myvar: {{ myvar.stdout }}"
```

Lancez le playbook localement:
```bash
ansible-playbook -i localhost, -c local playbook.yml  -k
```

## Navigation
* [Accueil - Ansible Labs](/README.md)
