# COOKIECUTTER

## c'est quoi?
[Cookiecutter](https://github.com/audreyr/cookiecutter) un outils pour créer des templates.

## Dans le cadre d'Ansible?
Il va nous servir à déployer un template de **role**


## Usage
```bash
python3 -m pip install cookiecutter
cookiecutter https://gitlab.com/kumaconsulting/ansible-role-template.git
```
## Autheur
©2022 Kuma-Consulting - Jérémie CUADRADO (redbeard28)

## Licence
Tous droits réservés à Jérémie CUADRADO

Toutes les marques citées appartiennent à leurs propriétaires; marques de tiers, noms de produits, noms commerciaux, dénominations sociales et noms de société cités sont des marques commerciales de leurs propriétaires respectifs ou des marques déposées d'autres sociétés et ne sont utilisés que pour des fins d'illustration et au bénéfice de leur propriétaire, sans que cela n'implique d'aucune manière une violation du droit d'auteur. 

